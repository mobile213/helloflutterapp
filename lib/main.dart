import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget  {
  @override
  _MyStateFulWigetState createState() => _MyStateFulWigetState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Aleteo";
String japanishGreeting = "こんにちはフラッター";
String thaiGreeting = "สวัสดีฟลัตเตอร์";


class _MyStateFulWigetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title: Text("Flutter"),
        leading: Icon(Icons.home),
        actions: <Widget>[
          IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting? spanishGreeting : englishGreeting;
                });
              },
              icon: Icon(Icons.translate)),
          IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting? japanishGreeting : englishGreeting;
                });
              },
              icon: Icon(Icons.g_translate)),
          IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting? thaiGreeting : englishGreeting;
                });
              },
              icon: Icon(Icons.refresh))
        ],

      ),
      body: Center(
        child: Text(
          displayText,
        style: TextStyle(fontSize: 24),
        ),
      ),
    ),
    );
    }
}



















// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
      // debugShowCheckedModeBanner: false
      // home: Scaffold(
      //   appBar: AppBar(
      //     title: Text("Flutter"),
      //     leading: Icon(Icons.home),
      //     actions: <Widget>[
      //       IconButton(
      //           onPressed: () {},
      //           icon: Icon(Icons.refresh))
      //     ],
      //   ),
      //   body: Center(
      //     child: Text(
      //       "Hello Flutter ",
      //     style: TextStyle(fontSize: 24),
      //     ),
      //   ),
      // ),
    // );
  // }
// }